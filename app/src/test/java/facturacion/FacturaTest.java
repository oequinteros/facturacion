package facturacion;

import org.junit.Test;
import static org.junit.Assert.*;

import java.time.LocalDate;

public class FacturaTest {
    @Test
    public void crearFactura(){
        FacturaVenta facturaVenta = new FacturaVenta();

        assertEquals(new Float(0), facturaVenta.totalFactura());
    }

    @Test
    public void agregarUnProductoAFacturar(){
        FacturaVenta facturaVenta = new FacturaVenta();
        Producto producto = new Producto("01","AA24","Producto A",10,10.5f);

        facturaVenta.agregarAFacturar(producto,2);

        assertEquals(new Float(21), facturaVenta.totalFactura());

    }
    @Test
    public void agregarUnProductoPerecederoAFacturar(){
        FacturaVenta facturaVenta = new FacturaVenta();
        ProductoPerecedero producto = new ProductoPerecedero("01","AA24","Producto A",10,10.5f,LocalDate.now());

        facturaVenta.agregarAFacturar(producto,2);

        assertEquals(new Float(21), facturaVenta.totalFactura());

    }

    @Test
    public void agregarUnServicioAFacturar(){
        FacturaVenta facturaVenta = new FacturaVenta();
        Servicio servicio = new Servicio("01","Mano de Obra Cambio de Aceite",10.5f);

        facturaVenta.agregarAFacturar(servicio,2);

        assertEquals(new Float(21), facturaVenta.totalFactura());

    }    
}
