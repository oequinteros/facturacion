package facturacion;

import java.time.LocalDate;

public class ProductoPerecedero extends Producto{
    private LocalDate fechaVencimiento;

    public ProductoPerecedero(String codigo, String ubicacion, String denoninacion, Integer stock, Float precioUnitario, LocalDate fechaVencimiento){
        super(codigo,ubicacion,denoninacion,stock,precioUnitario);
        this.fechaVencimiento=fechaVencimiento;
    }

}
