package facturacion;

public class Producto implements Facturable{
    private String codigo;
    private String ubicacion;
    private String denominacion;
    private Integer stock;
    private Float precioUnitario;

    public Producto(String codigo, String ubicacion, String denoninacion, Integer stock, Float precioUnitario){
        this.codigo=codigo;
        this.ubicacion=ubicacion;
        this.denominacion=ubicacion;
        this.stock=stock;
        this.precioUnitario=precioUnitario;
    }

    public Float getPrecioUnitario(){
        return precioUnitario;
    }
}
