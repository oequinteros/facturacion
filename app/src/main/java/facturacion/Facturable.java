package facturacion;

public interface Facturable {
    public Float getPrecioUnitario();
}
