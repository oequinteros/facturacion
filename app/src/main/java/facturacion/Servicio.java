package facturacion;

public class Servicio implements Facturable {
    private String codigo;
    private String denominacion;
    private Float precioUnitario;

    public Servicio(String codigo, String denoninacion, Float precioUnitario){
        this.codigo=codigo;
        this.denominacion=denoninacion;
        this.precioUnitario=precioUnitario;
    }

    public Float getPrecioUnitario(){
        return precioUnitario;
    }
}
