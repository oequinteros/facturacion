package facturacion;

import java.time.LocalDate;
import java.util.ArrayList;

public class FacturaVenta {


    private ArrayList<ItemFacturaVenta> items = new ArrayList<ItemFacturaVenta>();

    public Float totalFactura() {
        Float total = new Float(0);
        for (ItemFacturaVenta itemFacturaVenta : items) {
            total = total + itemFacturaVenta.total();
        }
        return total;
    }

    public void agregarAFacturar(Facturable producto, int cantidad) {
        items.add(new ItemFacturaVenta(producto,cantidad));    
    }

    private class ItemFacturaVenta {
        private Facturable producto;
        private Integer cantidad;
    
        public ItemFacturaVenta(Facturable producto, int cantidad) {
            this.producto=producto;
            this.cantidad=cantidad;
        }

        public Float total(){
            return cantidad*producto.getPrecioUnitario();
        }
    
    }    
}
