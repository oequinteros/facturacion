package facturacion;

public class Comercio {

    private static Comercio comercio;

    LibroVenta libro = new LibroVenta();
    AgendaCliente agendaCliente = new AgendaCliente();

    private Comercio(){

    }

    public static Comercio intancia(){
        if (comercio == null) {
            comercio = new Comercio();
        }
        return comercio;
    }
}
